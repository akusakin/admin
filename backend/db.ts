import { connect } from "https://deno.land/x/cotton@v0.7.5/mod.ts";
import { config } from "https://deno.land/x/dotenv@v2.0.0/mod.ts";
const {HOST, USERNAME, PASSWORD, PORT} = config()

const connection = await connect({
    type: "mysql",
    port: +PORT,
    database: 'Adminka',
    hostname: HOST,
    username: USERNAME,
    password: PASSWORD,
});

export const manager = connection.getManager()
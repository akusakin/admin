import { Schema } from "https://deno.land/x/cotton@v0.7.5/mod.ts";

export async function up(schema: Schema) {
    await schema.createTable('users', table => {
        table.id();
        table.varchar('email').unique();
        table.varchar('login').unique();
        table.varchar('password');
        table.varchar('profession');
        table.varchar('username');
        table.varchar('role');
    })
}

export async function down(schema: Schema) {
    await schema.dropTable('users');
}

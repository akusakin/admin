import {Application} from 'https://deno.land/x/oak@v6.5.0/mod.ts'
import {oakCors} from "https://deno.land/x/cors@v1.2.1/mod.ts";
import {routerUser, routerAdmin, router} from './src/routes/routes.ts'

const app = new Application()

app.use(oakCors({
    credentials: true, // чтобы frontend смог посылать нам куки
    origin: /^.+localhost:4200$/,
}))

app
    .use(routerUser.routes())
    .use(routerUser.allowedMethods())
    .use(routerAdmin.routes())
    .use(routerAdmin.allowedMethods())
    .use(router.routes())
    .use(router.allowedMethods())

app.listen({
    port: 8000
})
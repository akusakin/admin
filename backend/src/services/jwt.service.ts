import {create, verify, getNumericDate} from 'https://deno.land/x/djwt@v2.2/mod.ts'
import {Cookies} from 'https://deno.land/x/oak@v6.5.0/mod.ts'
import { config } from "https://deno.land/x/dotenv@v2.0.0/mod.ts";
import {Payload} from "../models/payload.ts";
import {Role} from "../models/role.ts";

const {SECRET_KEY} = config()

export class JwtService {
    async create(userId: number, role: Role) {
        const payload = {
            id: userId,
            role,
            exp: getNumericDate(60 * 10) // выдаём ключ на 10 минут
        };
        return await create({ alg: 'HS512', typ: 'JWT' }, payload, SECRET_KEY)
    }

    async verify(cookies: Cookies): Promise<Payload|false> {
        const token = cookies.get('token')
        if (!token) {
            return Promise.resolve(false)
        }

        let payload
        try {
            payload = await verify(token, SECRET_KEY, 'HS512') as unknown as Payload
        } catch (e) {
            // токен протух
            return Promise.resolve(false)
        }

        return payload
    }
}

import {AuthController} from "../controllers/auth.controller.ts";
import {UserController} from "../controllers/user.controller.ts";
import {UserAuthMiddleware} from "../middlewares/user-auth.middleware.ts";
import {AdminAuthMiddleware} from "../middlewares/admin-auth.middleware.ts";
import {JwtService} from "./jwt.service.ts";
import {UserService} from "./user.service.ts";

const jwtService = new JwtService()
const userService = new UserService()
export const authController = new AuthController(jwtService, userService)
export const userController = new UserController(userService)
export const userAuthMiddleware = new UserAuthMiddleware(jwtService)
export const adminAuthMiddleware = new AdminAuthMiddleware(jwtService)

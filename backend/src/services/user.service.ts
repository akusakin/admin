import {hash} from "https://deno.land/x/bcrypt@v0.2.4/mod.ts";
import {User} from "../entities/table-users.ts";
import {IUser} from "../models/user.ts";
import {UserDb} from "../entities/user-db.ts";
import {Role} from "../models/role.ts";
import {manager} from '../../db.ts'

export class UserService {
    async isExist(email: string): Promise<boolean> {
        return !!await this.getUserDb(email)
    }

    async create({
         email,
         login,
         password,
         profession,
         username,
         role
    }: {
        email: string,
        login: string,
        password: string,
        profession: string,
        username: string,
        role: Role
    }): Promise<number> {
        const user = new User()
        const password_hash = await hash(password)

        user.email = email
        user.login = login
        user.password = password_hash
        user.profession = profession
        user.username = username
        user.role = role

        const createdUser = await manager.save(user)
        return createdUser.id
    }

    async getUserDb(email: string): Promise<UserDb|null> {
        return await manager.query(User).where('email', email).first()
    }

    async get(id: number): Promise<IUser|undefined> {
        const userDb = await manager.query(User).where("id", id).first();
        return userDb ? this.normilize(userDb) : Promise.resolve(undefined)
    }

    async getUsers(): Promise<IUser[]> {
        const usersDb = await manager.query(User).all();
        return usersDb.map((userDb: UserDb) => this.normilize(userDb))
    }

    async update(userId: number, newData: {
        [key: string]: any
    }) {
        const errors = []
        const allUsers = await this.getUsers()

        if (newData.email) {
            const isEmailAlreadyExist = !!allUsers.find(x => x.email === newData.email)
            if (isEmailAlreadyExist) {
                errors.push('email')
            }
        }

        if (newData.login) {
            const isLoginAlreadyExist = !!allUsers.find(x => x.login === newData.login)
            if (isLoginAlreadyExist) {
                errors.push('login')
            }
        }

        if (!errors.length) {
            await manager.query(User).where('id', userId).update(newData)
            return
        }

        return errors
    }

    async delete(id: string) {
        await manager.query(User).where('id', id).delete();
    }

    private normilize(userDb: UserDb): IUser {
        const {
            password,
            ...data
        } = userDb
        return data
    }
}

import {RouterContext} from 'https://deno.land/x/oak@v6.5.0/mod.ts'
import {JwtService} from "../services/jwt.service.ts";

export class UserAuthMiddleware {
    constructor(private jwtService: JwtService) {}

    async verify({response, cookies, state}: RouterContext, next: Function) {
        const payload = await this.jwtService.verify(cookies)
        if (payload) {
            state['user'] = payload
            await next()
            return
        }

        // либо токена нету, либо он протух
        response.status = 401;
        response.body = {
            message: 'Unauthenticated'
        }
    }
}

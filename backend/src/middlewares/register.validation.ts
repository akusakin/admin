import {RouterContext} from 'https://deno.land/x/oak@v6.5.0/mod.ts'
import { validate, required, isEmail } from "https://deno.land/x/validasaur@v0.15.0/mod.ts";

export const RegisterValidation = async ({request, response}: RouterContext, next: Function) => {
    const body = await request.body().value;
    const [passes, errors] = await validate(body, {
        email: [required, isEmail],
        login: required,
        username: required,
        profession: required,
        password: required,
        password_confirm: required
    });

    if (!passes) {
        response.status = 400
        response.body = errors
        return
    }

    if (body.password === body.password_confirm) {
        await next()
        return
    }

    response.status = 400
    response.body = {
        message: "Password wasn't confirm"
    }
}

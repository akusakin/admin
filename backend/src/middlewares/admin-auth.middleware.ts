import {RouterContext} from 'https://deno.land/x/oak@v6.5.0/mod.ts'
import {JwtService} from "../services/jwt.service.ts";
import {Role} from "../models/role.ts";

export class AdminAuthMiddleware {
    constructor(private jwtService: JwtService) {}

    async verify({response, cookies, state}: RouterContext, next: Function) {
        const payload = await this.jwtService.verify(cookies)
        if (!payload) {
            // либо токена нету, либо он протух
            response.status = 401;
            response.body = {
                message: 'Unauthenticated'
            }
            return
        }

        if (payload.role === Role.admin) {
            state['user'] = payload
            await next()
            return
        }

        response.status = 403;
        response.body = {
            message: 'Forbidden'
        }
    }
}

import { Model, Primary, Column, DataType } from "https://deno.land/x/cotton@v0.7.5/mod.ts";
import { Role } from '../models/role.ts'

@Model('users')
export class User {
    @Primary()
    id!: number

    @Column({ type: DataType.String })
    email!: string

    @Column({ type: DataType.String })
    login!: string

    @Column({ type: DataType.String })
    password!: string

    @Column({ type: DataType.String })
    profession!: string

    @Column({ type: DataType.String })
    username!: string

    @Column({ type: DataType.String })
    role!: Role 
}
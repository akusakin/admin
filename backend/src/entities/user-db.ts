import {Role} from "../models/role.ts";

export interface UserDb {
    id: number,
    email: string,
    login: string,
    password: string,
    profession: string,
    username: string,
    role: Role
}

import {Role} from "./role.ts";

export interface Payload {
    id: number,
    role: Role,
    exp: number
}

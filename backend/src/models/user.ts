import {Role} from "./role.ts";

export interface IUser {
    id: number,
    email: string,
    login: string,
    profession: string,
    username: string,
    role: Role
}

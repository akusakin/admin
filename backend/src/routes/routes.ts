import {Router} from 'https://deno.land/x/oak@v6.5.0/mod.ts'
import {RegisterValidation} from "../middlewares/register.validation.ts";
import {LoginValidation} from "../middlewares/login.validation.ts";
import {UserCreateValidation} from "../middlewares/user-create.validation.ts";
import {authController, userAuthMiddleware, adminAuthMiddleware, userController} from "../services/index.ts";

const router = new Router()
router.prefix('/api')
router
    .post('/register', RegisterValidation, c => authController.register(c))
    .post('/login', LoginValidation, c => authController.login(c))

const routerUser = new Router()
routerUser.prefix('/api')
routerUser.use((c, n) => userAuthMiddleware.verify(c, n))
routerUser
    .get('/user', c => userController.getCurrentUser(c))
    .put('/users/info', c => userController.updateInfo(c))
    .put('/users/password', c => userController.updatePassword(c))
    .post('/logout', c => authController.logout(c))

const routerAdmin = new Router()
routerAdmin.prefix('/api')
routerAdmin.use((c, n) => adminAuthMiddleware.verify(c, n))
routerAdmin
    .get('/users', c => userController.getUsers(c))
    .post('/users', UserCreateValidation, c => userController.create(c))
    .get('/users/:id', c => userController.getUser(c))
    .put('/users/:id', c => userController.updateUserInfo(c))
    .delete('/users/:id', c => userController.delete(c))

export {routerUser, routerAdmin, router}

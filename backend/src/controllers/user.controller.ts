import {RouterContext} from 'https://deno.land/x/oak@v6.5.0/mod.ts';
import {UserService} from "../services/user.service.ts";
import {hash} from "https://deno.land/x/bcrypt@v0.2.4/mod.ts";
import {Role} from "../models/role.ts";

export class UserController {
    constructor(private userService: UserService) {}

    async getCurrentUser({response, state}: RouterContext) {
        if (state.user) {
            response.body = await this.userService.get(state.user.id)
            return
        }

        // либо токена нету, либо он протух
        response.status = 401;
        response.body = {
            message: 'Unauthenticated'
        }
    }

    async getUsers({response}: RouterContext) {
        response.body = await this.userService.getUsers()
    }

    async getUser({response, params}: RouterContext) {
        const user = await this.userService.get(+params.id!)
        if (user) {
            response.body = user
            return
        }

        response.status = 404;
        response.body = {
            message: "User doesn't exist"
        }
    }

    async create({request, response}: RouterContext) {
        const {email, login, profession, username, role} = await request.body().value
        const password = '1' // по умолчанию для всех созданных админом пользователей пароль будет равен '1'
        const id = await this.userService.create({
            email,
            login,
            password,
            profession,
            username,
            role
        })
        response.body = {
            email,
            id,
            login,
            profession,
            username,
            role
        }
    }

    async updateUserInfo({response, request, params, state}: RouterContext) {
        if (state.user.role !== Role.admin) {
            response.status = 403;
            response.body = {
                message: 'Forbidden'
            }
            return
        }

        const body = await request.body().value
        const errors = await this.userService.update(+params.id!, body)
        if (errors) {
            response.body = {
                message: `Next field are already in use: ${errors}`
            }
            return
        }

        response.status = 204;
        response.body = {
            message: 'User updated successfully'
        }
    }

    async updateInfo({response, request, state}: RouterContext) {
        const body = await request.body().value
        const errors = await this.userService.update(state.user.id, body)
        response.body = errors ? {
            message: `Next field are already in use: ${errors}`
        } : await this.userService.get(state.user.id)
    }

    async updatePassword({response, request, state}: RouterContext) {
        const {password, password_confirm} = await request.body().value

        if (password !== password_confirm) {
            response.status = 400
            response.body = {
                message: "Password wasn't confirm"
            }
            return
        }

        await this.userService.update(state.user.id, {
            password: await hash(password)
        });

        response.body = {
            message: "Password updated successfully"
        }
    }

    async delete({response, params}: RouterContext) {
        await this.userService.delete(params.id!)
        response.status = 204;
    }
}

import {RouterContext} from 'https://deno.land/x/oak@v6.5.0/mod.ts'
import {compareSync} from "https://deno.land/x/bcrypt@v0.2.4/mod.ts";
import {JwtService} from "../services/jwt.service.ts";
import {UserService} from "../services/user.service.ts";
import {Role} from "../models/role.ts";

export class AuthController {
    constructor(private jwtService: JwtService, private userService: UserService) {}

    async register({request, response, cookies}: RouterContext) {
        const {email, login, username, profession, password} = await request.body().value;
        const isUserExist = await this.userService.isExist(email)

        if (isUserExist) {
            response.status = 404;
            response.body = {
                message: "this email address is already in use"
            }
            return
        }

        const userId = await this.userService.create({
            email,
            login,
            password,
            profession,
            username,
            role: Role.user
        })

        const token = await this.jwtService.create(userId, Role.user)

        cookies.set('token', token, {
            httpOnly: true, // чтобы нельзя было получить доступ к cookie через javascript
        })

        response.body = {
            token
        }
    }

    async login({request, response, cookies}: RouterContext) {
        const payload = await this.jwtService.verify(cookies)
        if (payload) {
            response.body = {
                token: cookies.get('token')
            }
            return
        }

        const {email, password} = await request.body().value;
        const userDb = await this.userService.getUserDb(email);
        if (!userDb) {
            response.status = 404;
            response.body = {
                message: "User not found"
            }
            return
        }

        if (!compareSync(password, userDb.password)) {
            response.status = 401;
            response.body = {
                message: "Incorrect password"
            }
            return
        }

        const token = await this.jwtService.create(userDb.id, userDb.role)

        cookies.set('token', token, {
            httpOnly: true, // чтобы нельзя было получить доступ к cookie через javascript
        })

        response.body = {
            token
        }
    }

    logout({response, cookies}: RouterContext) {
        cookies.delete('token')
        response.body = {
            message: 'success'
        }
    }
}

import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    updateInfo(data: {
        email: string,
        login: string,
        profession: string,
        username: string,
    }) {
        return fetch(`${environment.api}/users/info`, {
            method: 'PUT',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
    }

    getUser() {
        return fetch(`${environment.api}/user`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
    }

    getUsers() {
        return fetch(`${environment.api}/users`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
    }

    delete(id: number) {
        return fetch(`${environment.api}/users/${id}`, {
            method: 'DELETE',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
        })
    }

    create(data: {
        email: string,
        login: string,
        username: string,
        profession: string,
    }) {
        return fetch(`${environment.api}/users`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then(x => x.json())
    }
}

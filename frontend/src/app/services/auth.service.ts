import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    login(data: {
        email: string,
        password: string
    }) {
        return fetch(`${environment.api}/login`, {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
    }

    register(data: {
        email: string,
        login: string,
        username: string,
        profession: string,
        password: string,
        password_confirm: string
    }) {
        return fetch(`${environment.api}/register`, {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
    }

    logout() {
        return fetch(`${environment.api}/logout`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
    }

    updatePassword(data: {
        password: string,
        password_confirm: string
    }) {
        return fetch(`${environment.api}/users/password`, {
            method: 'PUT',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
    }
}

import {Role} from './role';

export interface User {
    id: number;
    email: string;
    login: string;
    profession: string;
    username: string;
    role: Role;
}

import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {User} from '../../models/user';
import {Role} from '../../models/role';
import {UsersService} from '../../services/users.service';

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
    isLoading = true
    role = Role
    user: User
    users: User[] = []

    info = {
        email: '',
        login: '',
        profession: '',
        username: ''
    }

    passwordData = {
        password: '',
        password_confirm: '',
    }

    newUser = {
        email: '',
        login: '',
        username: '',
        profession: '',
        role: '',
    }

    constructor(private authService: AuthService, private router: Router, private usersService: UsersService) { }

    ngOnInit(): void {
        this.usersService.getUser().then(response => {
            if (response.message === 'Unauthenticated') {
                this.router.navigate(['/login'])
                return
            }

            if (response.role === Role.admin) {
                this.getUsers(response)
                return
            }

            this.user = response
            this.isLoading = false
        })
    }

    getUsers(currentUser: User) {
        this.usersService.getUsers().then(response => {
            if (response.message === 'Unauthenticated') {
                this.router.navigate(['/login'])
                return
            }

            this.user = currentUser
            this.users = response
            this.isLoading = false
        })
    }

    logout() {
        this.authService.logout()
            .then(() => this.router.navigate(['/login']))
    }

    updateInfo() {
        this.usersService.updateInfo(this.info)
            .then(response => {
                if (response.message === 'Unauthenticated') {
                    this.router.navigate(['/login'])
                    return
                }

                this.user = response
                this.clearInfo()
            });
    }

    clearInfo() {
        this.info = {
            email: '',
            login: '',
            profession: '',
            username: ''
        }
    }

    updatePassword() {
        this.authService.updatePassword(this.passwordData)
            .then(response => {
                if (response.message === 'Unauthenticated') {
                    this.router.navigate(['/login'])
                    return
                }
                this.clearPassword()
            });
    }

    clearPassword() {
        this.passwordData = {
            password: '',
            password_confirm: '',
        }
    }

    deleteUser(userId: number) {
        this.usersService.delete(userId).then(() => {
            this.users = this.users.filter(x => x.id !== userId)
        })
    }

    createUser() {
        this.usersService.create(this.newUser).then((user: User) => {
            this.users = [...this.users, user]
        })
    }
}

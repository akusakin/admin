import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    email = '';
    password = '';

    constructor(private router: Router, private authService: AuthService) { }

    ngOnInit(): void {
    }

    submitForm(): void {
        this.authService.login({
            email: this.email,
            password: this.password
        })
            .then(() => this.router.navigate(['/info']));
    }

    goToRegister() {
        this.router.navigate(['/register'])
    }
}

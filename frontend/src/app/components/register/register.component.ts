import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  email = '';
  login = '';
  username = '';
  profession = '';
  password = '';
  passwordConfirm = '';

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  submitForm() {
    this.authService.register({
      email: this.email,
      login: this.login,
      username: this.username,
      profession: this.profession,
      password: this.password,
      password_confirm: this.passwordConfirm
    }).then(() => this.router.navigate(['/info']))
  }

}
